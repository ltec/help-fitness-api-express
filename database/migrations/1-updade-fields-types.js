'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "Goals", deps: []
 * createTable "Users", deps: []
 * createTable "validation_codes", deps: []
 *
 **/

var info = {
    "revision": 1,
    "name": "updade-fields-types",
    "created": "2019-07-20T02:30:27.433Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "createTable",
        params: [
            "Goals",
            {
                "id": {
                    "type": Sequelize.INTEGER,
                    "field": "id",
                    "autoIncrement": true,
                    "primaryKey": true,
                    "allowNull": false
                },
                "description": {
                    "type": Sequelize.STRING,
                    "field": "description"
                }
            },
            {}
        ]
    },
    {
        fn: "createTable",
        params: [
            "Users",
            {
                "id_user": {
                    "type": Sequelize.UUID,
                    "field": "id_user",
                    "defaultValue": "55df1390-aa96-11e9-9193-cf63f601cdcb",
                    "primaryKey": true,
                    "allowNull": false
                },
                "name": {
                    "type": Sequelize.STRING(70),
                    "field": "name"
                },
                "password": {
                    "type": Sequelize.STRING(50),
                    "field": "password"
                },
                "email": {
                    "type": Sequelize.STRING(50),
                    "field": "email"
                },
                "cell_phone": {
                    "type": Sequelize.STRING(50),
                    "field": "cell_phone"
                },
                "method": {
                    "type": Sequelize.STRING(20),
                    "field": "method"
                },
                "genre": {
                    "type": Sequelize.STRING(1),
                    "field": "genre"
                },
                "date_birth": {
                    "type": Sequelize.DATE,
                    "field": "date_birth"
                },
                "active": {
                    "type": Sequelize.BOOLEAN,
                    "field": "active",
                    "defaultValue": false
                }
            },
            {}
        ]
    },
    {
        fn: "createTable",
        params: [
            "validation_codes",
            {
                "id_code": {
                    "type": Sequelize.UUID,
                    "field": "id_code",
                    "defaultValue": "55dfd6e0-aa96-11e9-9193-cf63f601cdcb",
                    "primaryKey": true,
                    "allowNull": false
                },
                "id_user": {
                    "type": Sequelize.UUID,
                    "field": "id_user",
                    "defaultValue": "55dfd6e1-aa96-11e9-9193-cf63f601cdcb",
                    "allowNull": false
                },
                "code": {
                    "type": Sequelize.STRING(4),
                    "field": "code"
                },
                "active": {
                    "type": Sequelize.BOOLEAN,
                    "field": "active",
                    "defaultValue": false
                }
            },
            {}
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
