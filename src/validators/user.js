const { check } = require('express-validator/check');

exports.register = [
    check('email')
        .not()
        .isEmpty()
        .isEmail()
        .withMessage('Informe um email válido'),
    check('password')
        .not()
        .isEmpty()
        .withMessage('Informe um password válido')
        .isLength({ min: 8, max: 12}),
        //.withMessage('password deve ser entre 8 a 12 digitos'),
    check('name')
        .not()
        .isEmpty()
        .withMessage('Informe um nome válido'),
    check('date_birth')
        .not()
        .isEmpty()
        .withMessage('Informe a data de nascimento válido'),
    check('cell_phone')
        .not()
        .isEmpty()
        .withMessage('Informe a número de celular válido'),
    check('genre')
        .not()
        .isEmpty()
        .withMessage('Informe um genero válido')
];

exports.login = [
    check('email')
        .not()
        .isEmpty()
        .isEmail()
        .withMessage('Informe um email'),
    check('password')
        .not()
        .isEmpty()
        .withMessage('Informe um password')
];

exports.activateUser = [
    check('id_user')
        .not()
        .isEmpty()
        .isUUID()
        .withMessage('Informe o id do usuário.'),
    check('code')
        .isLength(4)
        .not()
        .isEmpty()
        .withMessage('Informe um código válido!')
];
