const { check } = require('express-validator/check');

exports.post = [
    check('name').isLength({ min: 3 }),
    check('email').isEmail(),
    check('age').isNumeric()
]
exports.postExercise = [
    check('muscle')
    .not()
    .isEmpty()
    .withMessage('Favor informar um musculo para o exercício!')
];