const express = require('express');
const app = express();
require('custom-env').env();

const swaggerUi = require('swagger-ui-express');
const swaggerDoc = require('../swagger.json');

app.use('/docs/v1/', swaggerUi.serve, swaggerUi.setup(swaggerDoc));

const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());

require('./passaport');

//Rotas
const index = require('./routes/index');
const goalsRoute = require('./routes/goals');
const muscleRoute = require('./routes/muscles');
const exercisesRoute = require('./routes/exercises');
const myTrainingsRoute = require('./routes/myTrainings');
const imagesRoute = require('./routes/images');
const auth = require('./routes/user');

app.use('/', index);
app.use('/goals', goalsRoute);
app.use('/goals', muscleRoute);
app.use('/goals', exercisesRoute);
app.use('/myTrainings', myTrainingsRoute);
app.use('./images', imagesRoute);
app.use('/login', auth);


module.exports = app;