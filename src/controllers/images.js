var path = require('path');

exports.getGoal = (req, res, next) => {
    let id = req.params.id;
    res.status(200).sendFile(path.resolve(__dirname+`/../imgs/goals/${id}/${id}.png`));
};

exports.getMuscle = (req, res, next) => {
    let idGoal = req.params.idGoal;
    let id = req.params.id;
    res.status(200).sendFile(path.resolve(__dirname+`/../imgs/goals/${idGoal}/muscles/${id}/${id}.png`));
};

exports.getExercise = (req, res, next) => {
    let idGoal = req.params.idGoal;
    let idMuscle = req.params.idMuscle;
    let id = req.params.id;
    res.status(200).sendFile(path.resolve(__dirname+`/../imgs/goals/${idGoal}/muscles/${idMuscle}/exercises/${id}.png`));
};