const JWT = require('jsonwebtoken');
const { JWT_SECRET } = require('../../auth');
const { validationResult } = require('express-validator/check');
const { Users, validation_code } = require('../models');
const { sendMail } = require('../services/emailService');
const Chance = require('chance');
const chance = new Chance();

signToken = user => {
    return JWT.sign({
        iss: 'HelpFitness',
        sub: user.id,
        iat: new Date().getTime(), // current time
        exp: new Date().setDate(new Date().getDate() + 1) // current time + 1 day ahead
    }, JWT_SECRET);
}

module.exports = {
    register: async (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty())
            return res.status(400).json({ errors: errors.array() });

        const { email, password, name,
            cell_phone, genre, date_birth } = req.body;

        await Users.findOne({
            where: {
                email: email
            }
        }).then((user) => {

            if (user) {
                return res.status(403).json({ error: 'Email já esta em uso' });
            }

            return Users.count().then(c => {
                return Users.create({

                    email: email,
                    name: name,
                    password: password,
                    method: 'local',
                    cell_phone: cell_phone,
                    genre: genre,
                    date_birth: date_birth

                }).then(user => {

                    return validation_code.create({
                        id_user: user.id_user,
                        code: chance.string({ length: 4, pool: '0123456789' })
                    }).then(result => {
                        return sendMail(user.name, user.email, result.code).then(result => res.status(200).send('Usuário cadastrado com sucesso!'));
                    })

                }).catch(err => {
                    res.status(500).send(err.message)
                });

            })
        });
    },

    signUp: async (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty())
            return res.status(400).json({ errors: errors.array() });

        const { email, password } = req.body;

        await Users.findOne({
            where: {
                email: email,
                password: password
            }
        }).then((user) => {

            if (!user) {
                return res.status(403).json({ error: 'Login invalido' });
            }

            const token = signToken(user);
            res.status(200).json({ token });
        });
    },

    signIn: async (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty())
            return res.status(400).json({ errors: errors.array() });

        // Generate token
        const token = signToken(req.user);
        res.status(200).json({ token });
    },

    googleOAuth: async (req, res, next) => {
        // Generate token
        const token = signToken(req.user);
        res.status(200).json({ token });
    },

    facebookOAuth: async (req, res, next) => {
        // Generate token
        const token = signToken(req.user);
        res.status(200).json({ token });
    },

    secret: async (req, res, next) => {
        console.log('I managed to get here!');
        res.json({ secret: "resource" });
    },

    activateUser: async (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty())
            return res.status(400).json({ errors: errors.array() });

        const { id_user, code } = req.body;

        let userCode = await validation_code.findOne({
            where: {
                id_user: id_user,
                code: code
            }
        });

        if (!userCode)
            return res.status(404).send("Nenhum usuário localizado com os parametros informados.");

        try {

            let updateUser = await Users.update({ active: true},
                {where: {
                    id_user: id_user
                }});

           let updateCode = await validation_code.update({ active: true},
                {where: {
                    id_user: id_user,
                    code: code
                }});

                return res.status(200).send("Usuário ativado com sucesso.");

        } catch (err) {
            return res.status(err.statusCode || 500).json({ error: err.message });
        }
   
    }
}
