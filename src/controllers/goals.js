let { objetivos } = require('../data/trainings.json');
const { Goals } = require('../models');

exports.get = (req, res, next) => {
    let limit = req.query.limit;

    console.log(limit);

    if (limit) {
        Goals.findAll({
            limit: parseInt(limit)
        }).then(goals => {
            res.status(200).send(goals);
        });
    } else {
        Goals.findAll().then(goals => {
            res.status(200).send(goals);
        });
    }
};
exports.getById = (req, res, next) => {
    let id = req.params.id;
    res.status(200).send(objetivos.find(x => x.id == id));
};
exports.post = (req, res, next) => {
    try {
        let description = req.body.description;
        console.log(description);
        Goals.create({ description: description });
        res.status(201).send('Registro recebida com sucesso!');
    } catch (error) {
        res.status(500).send('error');
    }
    ;
};
exports.put = (req, res, next) => {
    let id = req.params.id;
    res.status(201).send(`Requisição recebida com sucesso! ${id}`);
};
exports.delete = (req, res, next) => {
    let id = req.params.id;
    res.status(200).send(`Requisição recebida com sucesso! ${id}`);
};