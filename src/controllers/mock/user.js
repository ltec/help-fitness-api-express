const JWT = require('jsonwebtoken');
const { JWT_SECRET } = require('../../../auth');
const { validationResult } = require('express-validator/check');
const { Users, validation_code } = require('../../models');
const { sendMail } = require('../../services/emailService');
const Chance = require('chance');
const chance = new Chance();
var { getDatabase, setDatabase } = require('../../data/database.js');

signToken = user => {
    return JWT.sign({
        iss: 'HelpFitness',
        sub: user.id,
        iat: new Date().getTime(), // current time
        exp: new Date().setDate(new Date().getDate() + 1) // current time + 1 day ahead
    }, JWT_SECRET);
}

module.exports = {
    register: async (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty())
            return res.status(400).json({ errors: errors.array() });

        try {
            let user = req.body;
            let data = getDatabase();
            let users = data.users;

            if (users.some(x => x.email == user.email)){
                return res.status(403).json({message: "Usuário já cadastrado."});
            }

            let id = users.length + 1;
            users.push({ id: id, ...user });
            setDatabase(data);
            res.status(200).json({message: "Usuário cadastrado com sucesso."});
        } catch (error) {
            res.status(500).json({message: "Não foi possível efetuar o cadastro, tente novamente mais tarde."});
        }  
    },

    signUp: async (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty())
            return res.status(400).json({ errors: errors.array() });

        try {
            const { email, password } = req.body;
            let data = getDatabase();
            let users = data.users;

            let user = users.find(x => x.email == email);

            if(!user){
                return res.status(403).json({ message: 'Login não cadastrado.' });
            } else if (user.password != password) {
                return res.status(401).json({ message: 'Usuário ou senha inválido.' });
            } else {
                const token = signToken(user);
                res.status(200).json({ token });
            }
        } catch (error) {
            res.status(500).json({message: "Não foi possível efetuar o login, tente novamente mais tarde."});
        }
    },

    signIn: async (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty())
            return res.status(400).json({ errors: errors.array() });

        // Generate token
        const token = signToken(req.user);
        res.status(200).json({ token });
    },

    googleOAuth: async (req, res, next) => {
        // Generate token
        const token = signToken(req.user);
        res.status(200).json({ token });
    },

    facebookOAuth: async (req, res, next) => {
        // Generate token
        const token = signToken(req.user);
        res.status(200).json({ token });
    },

    secret: async (req, res, next) => {
        console.log('I managed to get here!');
        res.json({ secret: "resource" });
    },

    activateUser: async (req, res, next) => {

        const errors = validationResult(req);
        if (!errors.isEmpty())
            return res.status(400).json({ errors: errors.array() });

        const { id_user, code } = req.body;

        let userCode = await validation_code.findOne({
            where: {
                id_user: id_user,
                code: code
            }
        });

        if (!userCode)
            return res.status(404).send("Nenhum usuário localizado com os parametros informados.");

        try {

            let updateUser = await Users.update({ active: true},
                {where: {
                    id_user: id_user
                }});

           let updateCode = await validation_code.update({ active: true},
                {where: {
                    id_user: id_user,
                    code: code
                }});

                return res.status(200).send("Usuário ativado com sucesso.");

        } catch (err) {
            return res.status(err.statusCode || 500).json({ error: err.message });
        }
   
    }
}
