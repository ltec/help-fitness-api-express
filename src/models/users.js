const uuidv1 = require('uuid/v1');

module.exports = (sequelize, DataTypes) => {
    const Users = sequelize.define('Users', {
        id_user: {
            allowNull: false,
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: uuidv1()
        },
        name: DataTypes.STRING(70),
        password: DataTypes.STRING(50),
        email: DataTypes.STRING(50),
        cell_phone: DataTypes.STRING(50),
        method: DataTypes.STRING(20),
        genre: DataTypes.STRING(1),
        date_birth: DataTypes.DATE,
        active: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    });
  
    return Users;
  }