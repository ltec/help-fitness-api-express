const uuidv1 = require('uuid/v1');

module.exports = (sequelize, DataTypes) => {
    const ValidationCode = sequelize.define('validation_code', {
        id_code: {
            allowNull: false,
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: uuidv1()
        },
        id_user:  {
            allowNull: false,
            type: DataTypes.UUID,
            defaultValue: uuidv1()
        },
        code: {
            type: DataTypes.STRING(4)
        },
        active: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    });
  
    return ValidationCode;
  }