var fs = require('fs');

exports.getDatabase = () => {
    let file = fs.readFileSync("./src/data/database.json", "utf8");
    return JSON.parse(file);
}

exports.setDatabase = (data) => {
    let json = JSON.stringify(data, null, 4);
    let file = fs.writeFile("./src/data/database.json", json, (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    });
    return file;
}