const express = require('express');
const router = express.Router();
const controller = require('../controllers/goals');
const verifyJWT = require('../verifyJwt');

router.get('/', verifyJWT, controller.get);

router.get('/:id', controller.getById);

router.post('/', controller.post);

router.put('/:id', controller.put);

router.delete('/:id', controller.delete);

module.exports = router;