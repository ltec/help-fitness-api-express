
const express = require('express');
const router = express.Router();
const passport = require('passport');

const validation = require('../validators/user');
const UsersController = require('../controllers/user');
const passportJWT = passport.authenticate('jwt', { session: false });
const passportSignIn = passport.authenticate('local', { session: false });

const mockController = require('../controllers/mock/user');

router.post('/register/mock', validation.register, mockController.register);

router.post('/register', validation.register, UsersController.register);

router.post('/signup/mock', validation.login, mockController.signUp);

router.post('/signup', validation.login, UsersController.signUp);

router.post('/signin', validation.login, passportSignIn, UsersController.signIn);

router.post('/login/mock', validation.register, mockController.register);

router.post('/oauth/google', passport.authenticate('googleToken', { session: false }), UsersController.googleOAuth);

router.post('/oauth/facebook', passport.authenticate('facebookToken', { session: false }), UsersController.facebookOAuth);

router.get('/secret', passportJWT, UsersController.secret);

router.post('/activateUser', validation.activateUser, UsersController.activateUser)

module.exports = router;