const express = require('express');
const router = express.Router();
const controller = require('../controllers/images');

router.get(process.env.IMAGES_SUB_ROUTE + '/:id', controller.getGoal);

router.get(process.env.IMAGES_SUB_ROUTE + '/:idGoal/muscles/:id', controller.getMuscle);

router.get(process.env.IMAGES_SUB_ROUTE + '/:idGoal/muscles/:idMuscle/exercises/:id', controller.getExercise);

module.exports = router;