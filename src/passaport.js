const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const GooglePlusTokenStrategy = require('passport-google-plus-token');
const FacebookTokenStrategy = require('passport-facebook-token');
const LocalStrategy = require('passport-local');
const config = require('../auth');
//const Users = require('./models/users');
const { Users } = require('./models');
//const passportConf = require('../passport');

// JSON WEB TOKENS STRATEGY
passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config.JWT_SECRET
}, async (payload, done) => {
    try {
        // Find the user specified in token
        await Users.findByPk(payload.sub).then(user => {
            if (!user) {
                return done(null, false);
            }

            // Otherwise, return the user
            done(null, user);
        });

    } catch (error) {
        done(error, false);
    }
}));

// Google OAuth Strategy
passport.use('googleToken', new GooglePlusTokenStrategy({
    clientID: config.oauth.google.clientID,
    clientSecret: config.oauth.google.clientSecret
}, async (accessToken, refreshToken, profile, done) => {
    try {
        // Should have full user profile over here
        console.log('profile', profile);
        console.log('accessToken', accessToken);
        console.log('refreshToken', refreshToken);

        await Users.findOrCreate({
            where: {
                id: profile.id,
                name: profile.displayName,
                email: profile.emails[0].value,
                method: 'google'
            }
        }).then(([user, created]) => {
            done(null, user);
        });
    } catch (error) {
        done(error, false, error.message);
    }
}));

passport.use('facebookToken', new FacebookTokenStrategy({
    clientID: config.oauth.facebook.clientID,
    clientSecret: config.oauth.facebook.clientSecret
}, async (accessToken, refreshToken, profile, done) => {
    try {
        console.log('profile', profile);
        console.log('accessToken', accessToken);
        console.log('refreshToken', refreshToken);

        await Users.findOrCreate({
            where: {
                id: profile.id,
                name: profile.displayName,
                email: profile.emails[0].value,
                method: 'facebook'
            }
        }).then(([user, created]) => {
            done(null, user);
        });
    } catch (error) {
        done(error, false, error.message);
    }
}));

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, async (username, password, done) => {

    await Users.findOne({
        where: {
            email: username,
            password: password
        }
    }).then((user) => {

     

        if (!user) {
            return done(new Error("Usuário não localizado!"));
        }
        else{
            return done(null, user);
            // if(user.active)
            // return done(null, user);
            
            // return done(null, false, {message: "Usuario não cadastrado"})
        }
        
    }).catch(err => {
        return err;
    });

}));