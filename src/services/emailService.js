const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: "helpfitness.dev@gmail.com",
        pass: "_Helpfitness@01"
    },
  //  tls: { rejectUnauthorized: false }
  });

  const mailOptions = {
    from: 'no-reply@helpfitness.com',
    to: 'leandro.sl.carvalho@gmail.com',
    subject: 'Código de ativação Help Fitness'
  };

const service = {};

service.sendMail = (name, userEmail, code) => {

   return new Promise((resolve, reject) => {

    mailOptions.html = `<h1>Help Fitness</h1><p>Olá ${name}, segue seu código de ativação: <b>${code}</b>.</p>`;
    mailOptions.to = userEmail;

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
          reject(error);
        } else {
          console.log('Email enviado: ' + info.response);
          resolve();
        }
      });

  });
};

module.exports = service;