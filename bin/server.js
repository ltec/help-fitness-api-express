require('custom-env').env();
const app = require('../src/app');
const port = normalizaPort(process.env.APP_PORT); 

function normalizaPort(val) {
    const port = parseInt(val, 10);
    if (isNaN(port)) {
        return val;
    }
if (port >= 0) {
        return port;
    }
return false;
}
app.listen(process.env.PORT || port, function () {
    console.log(`app listening on port ${port}`)
})